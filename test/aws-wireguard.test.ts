import { expect as expectCDK, haveResource, SynthUtils } from '@aws-cdk/assert';
import { Settings } from 'aws-cdk/lib/settings';

import cdk = require('@aws-cdk/core');
import AwsWireguard = require('../lib/aws-wireguard-stack');

/* eslint-disable no-undef */
test('Secret Created', () => {
  const app = new cdk.App();
  const stack = new AwsWireguard.AwsWireguardStack(app, 'AWSWireGuardTestStack');
  expectCDK(stack).to(haveResource('AWS::SecretsManager::Secret'));
});

test('Role Created', () => {
  const app = new cdk.App();
  const stack = new AwsWireguard.AwsWireguardStack(app, 'AWSWireGuardTestStack');
  expectCDK(stack).to(haveResource('AWS::IAM::Role'));
});

test('VPC Created', () => {
  const app = new cdk.App();
  const stack = new AwsWireguard.AwsWireguardStack(app, 'AWSWireGuardTestStack');
  expectCDK(stack).to(haveResource('AWS::EC2::VPC', {
    CidrBlock: '10.6.0.0/16', EnableDnsSupport: true,
  }));
});

test('Security Group Created', () => {
  const app = new cdk.App();
  const stack = new AwsWireguard.AwsWireguardStack(app, 'AWSWireGuardTestStack');
  expectCDK(stack).to(haveResource('AWS::EC2::SecurityGroup', {
    SecurityGroupIngress: [
      {
        CidrIp: '0.0.0.0/0',
        Description: 'allow WireGuard access',
        FromPort: 51820,
        IpProtocol: 'udp',
        ToPort: 51820,
      },
    ],
  }));
});

test('WireGuard Instance Created', () => {
  const app = new cdk.App();
  const stack = new AwsWireguard.AwsWireguardStack(app, 'AWSWireGuardTestStack');
  expectCDK(stack).to(haveResource('AWS::EC2::Instance', {
    InstanceType: 't2.micro', SourceDestCheck: false,
  }));
});

test('Snapshot', () => {
  const app = new cdk.App();
  const stack = new AwsWireguard.AwsWireguardStack(app, 'AWSWireGuardTestStack');
  expect(SynthUtils.toCloudFormation(stack)).toMatchSnapshot();
});

test('can parse string context from command line arguments', () => {
  // GIVEN
  const settings1 = Settings.fromCommandLineArguments({ context: ['foo=bar'] });
  const settings2 = Settings.fromCommandLineArguments({ context: ['foo='] });

  // THEN
  expect(settings1.get(['context']).foo).toEqual('bar');
  expect(settings2.get(['context']).foo).toEqual('');
});

test('can parse string context from command line arguments with equals sign in value', () => {
  // GIVEN
  const settings1 = Settings.fromCommandLineArguments({ context: ['foo==bar='] });
  const settings2 = Settings.fromCommandLineArguments({ context: ['foo=bar='] });

  // THEN
  expect(settings1.get(['context']).foo).toEqual('=bar=');
  expect(settings2.get(['context']).foo).toEqual('bar=');
});

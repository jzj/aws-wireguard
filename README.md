
# WireGuard VPN on an Amazon Linux 2 instance

This project is an application that uses the [AWS Cloud Development Kit (CDK)](https://aws.amazon.com/cdk/) to 
deploy an [AWS CloudFormation](https://aws.amazon.com/cloudformation/) 
[Stack](https://docs.aws.amazon.com/AWSCloudFormation/latest/UserGuide/cfn-whatis-concepts.html#w2ab1b5c15b9).  The
stack contains an Amazon Linux EC2 instance with [WireGuard](https://www.wireguard.com/) installed and configured for a 
single client.  The purpose of the EC2 instance is to facilitate a VPN connection from a WireGuard client to a 
WireGuard server.  Once connected to the server, the WireGuard client will route all Internet traffic through the EC2 
instance. This VPN is useful for a person than wants to isolate their Internet traffic from their local network or for 
[Road Warriors](https://en.wikipedia.org/wiki/Road_warrior_\(computing\)) that need an ad-hoc VPN connection to an 
existing VPC.

## Resources

This CDK application will deploy an instance of a CloudFormation Stack (`AwsWireguardStack`) which contains the 
following AWS resources:

* Secrets Manager Secret
  * For storing the public key server key.
* IAM Role 
  * Allows the EC2 instance access to update the public key value in secrets manager.
* Security Group
  * Allow ingress on a UDP port to the EC2 instance
  * Defaults to port 51820
* EC2 Instance
  * size: T2 Micro
  * ami: Latest Amazon Linux 2

![Resource Dependencies](docs/images/deps.png "Resource Dependencies")

## Prerequisites

* [Node.js](https://nodejs.org)
* CDK Toolkit: `npm i -g aws-cdk`
* WireGuard installed on a client.
* The WireGuard public key for the client.

### AWS Permissions

To deploy this stack, you will need permissions to create an EC2 instance, an IAM Role, and a secret in Secrets Manager.

## Setup

Clone the repository and run:

```console
$ cd aws-wireguard
$ npm install
```
## Build

To build the Typescript code run:

```console
$ npm run build
```

Or, to continuously build in the background:

```console
$ npm run watch
```

## Deploy

__If you choose to run this stack, you are responsible for any AWS costs incurred. The default values are 
cost-conscious.__

This stack relies on getting the public key for the peer client and other information from 
[CDK context](https://docs.aws.amazon.com/cdk/latest/guide/context.html). The public key for the peer client is 
required.  

You'll need to setup AWS credentials in your environment (e.g., via `aws configure`), and then execute:

 
```console
$ cdk deploy -c peerPublicKey="<insert peer client public key here>" --region <region>
```

Or add the following to cdk.json:

```json
{
  "context": {
    "peerPublicKey": "<insert peer client public key here>"
  }
}
```

then execute:

```console
$ cdk deploy --region <region>
```

To deploy to an existing VPC:

```console
$ cdk deploy -c peerPublicKey="<insert peer client public key here>" -c vpcId="<enter the VPC ID here>" \
  --region <region>
```

To restrict access to your IP (recommended):

```console
$ cdk deploy -c peerPublicKey="<insert peer client public key here>" -c ingressCidr="<enter your IPV4 address here>/32"
```

## Post Deployment 

Once the deployment is complete, the stack will output the following:

* The `aws` cli command for retrieving the public key for the server.
* The `wg` command for adding the server to the client.

## How to Connect to the WireGuard Instance

```console
$ wg-quick up wg0
```

## How to Disconnect from the WireGuard Instance

```console
$ wg-quick down wg0
```

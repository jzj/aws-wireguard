import ec2 = require('@aws-cdk/aws-ec2');
import iam = require('@aws-cdk/aws-iam');
import secretsmanager = require('@aws-cdk/aws-secretsmanager');
import cdk = require('@aws-cdk/core');
import fs = require('fs');
import util = require('util');

// eslint-disable-next-line import/prefer-default-export
export class AwsWireguardStack extends cdk.Stack {
  constructor(scope: cdk.App, id: string, props?: cdk.StackProps) {
    super(scope, id, props);
    const peerPublicKey: string = this.node.tryGetContext('peerPublicKey') ? this.node.tryGetContext('peerPublicKey') : '';
    const vpcId: string = this.node.tryGetContext('vpcId');
    const ingressCidr: string = this.node.tryGetContext('ingressCidr') ? this.node.tryGetContext('ingressCidr') : '0.0.0.0/0';
    const port: number = this.node.tryGetContext('port') ? this.node.tryGetContext('port') : 51820;
    const peerCidr: string = this.node.tryGetContext('peerCidr') ? this.node.tryGetContext('peerCidr') : '10.66.66.2/32';
    const serverCidr: string = this.node.tryGetContext('serverCidr') ? this.node.tryGetContext('serverCidr') : '10.66.66.1/24';
    const vpcCidr: string = this.node.tryGetContext('vpcCidr') ? this.node.tryGetContext('vpcCidr') : '10.6.0.0/16';
    let vpc: ec2.IVpc;
    const secret = new secretsmanager.Secret(this, 'WireGuardSecret');
    const role = new iam.Role(this, 'Role', {
      assumedBy: new iam.ServicePrincipal('ec2.amazonaws.com'),
    });

    role.addToPolicy(
      new iam.PolicyStatement(
        {
          actions: ['secretsmanager:GetSecretValue', 'secretsmanager:UpdateSecret',
            'secretsmanager:PutSecretValue', 'secretsmanager:DescribeSecret'],
          effect: iam.Effect.ALLOW,
          resources: [secret.secretArn],
        },
      ),
    );

    if (vpcId) {
      vpc = ec2.Vpc.fromLookup(this, 'VPC', {
        vpcId,
      });
    } else {
      vpc = new ec2.Vpc(this, 'VPC', {
        cidr: vpcCidr,
        maxAzs: 3,
        subnetConfiguration: [
          {
            cidrMask: 24,
            name: 'Ingress',
            subnetType: ec2.SubnetType.PUBLIC,
          },
        ],
      });
    }

    const wgSecurityGroup = new ec2.SecurityGroup(this, 'WgSecurityGroup', {
      allowAllOutbound: true,
      description: 'Allow WireGuard protocol to ec2 instances',
      vpc,
    });

    wgSecurityGroup.addIngressRule(
      ec2.Peer.ipv4(ingressCidr),
      ec2.Port.udp(port),
      'allow WireGuard access',
    );

    const amznLinux = new ec2.AmazonLinuxImage({
      edition: ec2.AmazonLinuxEdition.STANDARD,
      generation: ec2.AmazonLinuxGeneration.AMAZON_LINUX_2,
      storage: ec2.AmazonLinuxStorage.GENERAL_PURPOSE,
      virtualization: ec2.AmazonLinuxVirt.HVM,
    });

    const wgServer = new ec2.Instance(this, 'WireGuardServer', {
      instanceName: 'WireGuard Server',
      instanceType: ec2.InstanceType.of(ec2.InstanceClass.T2, ec2.InstanceSize.MICRO),
      machineImage: amznLinux,
      role,
      securityGroup: wgSecurityGroup,
      sourceDestCheck: false,
      vpc,
      vpcSubnets: {
        subnetType: ec2.SubnetType.PUBLIC,
      },
    });
    // The user data script adds WireGuard keys to Secrets Manager. secret must exists first.
    wgServer.node.addDependency(secret);
    const cfnWgServer: ec2.CfnInstance = wgServer.node.defaultChild as ec2.CfnInstance;
    cfnWgServer.cfnOptions.creationPolicy = { resourceSignal: { count: 1, timeout: 'PT20M' } };
    const scriptData: string = fs.readFileSync('scripts/install-wireguard.sh', 'utf8');
    wgServer.addUserData(
      util.format(
        scriptData, cfnWgServer.logicalId, this.stackName, secret.secretArn, serverCidr, peerCidr,
        peerPublicKey, port,
      ),
    );

    const awsGetPublicKeyCommand = `PUBLIC_KEY=$(aws secretsmanager get-secret-value --secret-id "${secret.secretArn}" --output text --query \\\n'SecretString' --region ${this.region})`;
    const wgPeerCommand = `sudo wg set wg0 peer $PUBLIC_KEY endpoint ${wgServer.instancePublicIp}:${port} allowed-ips 0.0.0.0/0 && \\\nsudo wg-quick save wg0`;
    /* eslint-disable no-new */
    new cdk.CfnOutput(this, 'WireGuardPublicIP', { value: wgServer.instancePublicIp });
    new cdk.CfnOutput(this, 'WireGuardPublicKey', { value: awsGetPublicKeyCommand });
    new cdk.CfnOutput(this, 'WireGuardPublicEndpoint', { value: wgPeerCommand });
  }
}

#!/usr/bin/env bash

# Setups a WireGuard VPN server on an Amazon Linux EC2 instance inside a VPC.

readonly LOGICAL_ID="%s"
readonly STACK_NAME="%s"
readonly SECRET_ID="%s"
readonly ADDRESS="%s"
readonly PEER_ALLOWED_IPS="%s"
readonly PEER_PUBLIC_KEY="%s"
readonly PORT="%d"
readonly AZ=$(curl -s http://169.254.169.254/latest/meta-data/placement/availability-zone)
readonly REGION=${AZ::-1}
REBOOT_REQUIRED=false

function command_is_installed() {
  local -r name="$1"
  command -v "$name" >/dev/null
}

function get_dns_server() {
  cidr=$(curl -sS \
    http://169.254.169.254/latest/meta-data/network/interfaces/macs/"$(curl -sS \
      http://169.254.169.254/latest/meta-data/mac)"/vpc-ipv4-cidr-block |
    cut -f 1 -d '/')
  [[ "$cidr" =~ ([0-9]+\.[0-9]+\.[0-9]+\.)([0-9]+)$ ]]
  echo "${BASH_REMATCH[1]}"${BASH_REMATCH[2]+2}
}

function send_cfn_init() {
  /opt/aws/bin/cfn-init --stack "${STACK_NAME}" --resource "${LOGICAL_ID}" --region "${REGION}"
}

function send_cfn_signal() {
  local -r exitCode=$1
  # shellcheck disable=SC2086
  /opt/aws/bin/cfn-signal -e $exitCode --stack "${STACK_NAME}" --resource "${LOGICAL_ID}" --region "${REGION}"
}

function update_os() {
  yum update -y -q
}

function install_cfn_helper_scripts() {
  yum install -y -q aws-cfn-bootstrap
}

function store_public_key() {
  aws configure set region "$REGION"
  echo "Storing public key in secretsmanger..."
  if ! aws secretsmanager put-secret-value --secret-id "${SECRET_ID}" \
    --secret-string "${PUBLIC_KEY}"; then
    send_cfn_signal $?
    echo "secretsmanger update failed!"
    exit 1
  fi
  echo "secretsmanger update completed."
}

function install_wireguard() {
  if ! command_is_installed "wg"; then
    echo "Installing Wireguard..."
    curl -sS -Lo /etc/yum.repos.d/wireguard.repo \
      https://copr.fedorainfracloud.org/coprs/jdoss/wireguard/repo/epel-7/jdoss-wireguard-epel-7.repo
    amazon-linux-extras install epel -y >/dev/null
    yum -y -q install wireguard-dkms wireguard-tools iptables kernel-headers-"$(uname -r)" kernel-devel-"$(uname -r)"
    if ! command_is_installed "wg"; then
      send_cfn_signal $?
      echo "WireGuard installed failed."
      exit 1
    fi
    echo "WireGuard installed successfully."
    echo "Loading wireguard..."
    if modprobe wireguard; then
      echo "Wireguard loaded."
    else
      REBOOT_REQUIRED=true
      echo "Wireguard failed to load. A reboot is required."
    fi
  fi
}

function configure_ip_forwarding() {
  if [[ ! -f /etc/sysctl.d/wg.conf ]]; then
    echo "Enable forwarding..."
    echo "net.ipv4.ip_forward = 1" >/etc/sysctl.d/wg.conf
    if ! sysctl --system; then
      send_cfn_signal $?
      echo "Enable forwarding failed."
      exit 1
    fi
    echo "Enabled forwarding was success."
  fi
}

function configure_interface() {
  if [[ ! -f /etc/wireguard/wg0.conf ]]; then
    echo "Generating the keys..."
    PRIVATE_KEY=$(wg genkey)
    PUBLIC_KEY=$(echo "$PRIVATE_KEY" | wg pubkey)
    echo "Key generation complete."
    echo "Creating wg0 configuration file..."
    DNS_SERVER=$(get_dns_server)
    mkdir -p /etc/wireguard
    chmod 700 /etc/wireguard/
    # Configure the interface and enable NAT on the server
    cat >/etc/wireguard/wg0.conf <<EOI

  [Interface]
  Address = ${ADDRESS}
  PrivateKey = ${PRIVATE_KEY}
  ListenPort = ${PORT}
  DNS = ${DNS_SERVER}
  SaveConfig = true
  PostUp = iptables -t nat -A POSTROUTING -o eth0 -j MASQUERADE
  PostDown = iptables -t nat -D POSTROUTING -o eth0 -j MASQUERADE

EOI

    if [[ "$PEER_PUBLIC_KEY" != "" ]]; then
      cat >>/etc/wireguard/wg0.conf <<EOP

  [Peer]
  PublicKey = ${PEER_PUBLIC_KEY}
  AllowedIPs = ${PEER_ALLOWED_IPS}

EOP
    fi

    chmod 600 /etc/wireguard/wg0.conf
    echo "Configuration file creation complete."
    systemctl enable wg-quick@wg0
    store_public_key
  fi
}

function reboot_if_required() {
  if [[ "$REBOOT_REQUIRED" == true ]]; then
    echo "Rebooting."
    # Exit successfully then reboot in 3 seconds
    sleep 3 && shutdown -r now "Rebooting to activate the WireGuard kernel module" &
    send_cfn_signal 0
    exit 0
  fi
}

function start_wireguard() {
  echo "Starting WireGuard..."
  if ! systemctl start wg-quick@wg0; then
    send_cfn_signal $?
    echo "The WireGuard server failed to start!"
    exit 1
  fi
  echo "WireGuard started."
}

function main() {
  echo "Starting WireGuard installation and configuration..."
  update_os
  install_cfn_helper_scripts
  send_cfn_init
  install_wireguard
  configure_ip_forwarding
  configure_interface
  reboot_if_required
  start_wireguard
  send_cfn_signal 0
  # Show WireGuard status information
  systemctl -l status wg-quick@wg0
  wg show wg0
  echo "WireGuard installation and configuration complete."
}

main

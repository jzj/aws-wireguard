#!/usr/bin/env node

import { AwsWireguardStack } from '../lib/aws-wireguard-stack';

import cdk = require('@aws-cdk/core');

const app = new cdk.App();
/* eslint-disable no-new */
new AwsWireguardStack(app, 'AwsWireguardStack', {
  env: {
    account: process.env.CDK_DEFAULT_ACCOUNT,
    region: process.env.CDK_DEFAULT_REGION,
  },
});
